//
//  FurbyTableViewCell.swift
//  FurbyIosApp
//
//  Created by Diederich Kroeske on 05/09/2018.
//  Copyright © 2018 Diederich Kroeske. All rights reserved.
//

import UIKit

class FurbyTableViewCell: UITableViewCell {

    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var phrase: UILabel!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
