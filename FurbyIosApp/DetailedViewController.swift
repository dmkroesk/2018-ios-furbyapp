//
//  DetailedViewController.swift
//  FurbyIosApp
//
//  Created by Diederich Kroeske on 05/09/2018.
//  Copyright © 2018 Diederich Kroeske. All rights reserved.
//

import UIKit

class DetailedViewController: UIViewController {

    var furby : Furby?
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var detailedAvatar: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        name.text = furby?.name
        detailedAvatar.image = UIImage(named: (furby?.imageUrl)!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
