//
//  Furby.swift
//  FurbyIosApp
//
//  Created by Diederich Kroeske on 05/09/2018.
//  Copyright © 2018 Diederich Kroeske. All rights reserved.
//

import Foundation

class Furby {
    
    var name : String?
    var generation: Int?
    var phrase : String?
    var imageUrl : String?
    
    required init(name :  String, generation: Int, phrase: String, imageUrl: String) {
        self.name = name
        self.generation = generation
        self.phrase = phrase
        self.imageUrl = imageUrl
    }
 }
