//
//  FurbyFactory.swift
//  FurbyIosApp
//
//  Created by Diederich Kroeske on 07/09/2018.
//  Copyright © 2018 Diederich Kroeske. All rights reserved.
//

import Foundation


// volgens: https://cocoacasts.com/what-is-a-singleton-and-how-to create-one-in-swift
class FurbyFactory {

    private static var sharedFurbyFactory: FurbyFactory = {
        let factory = FurbyFactory()
        return factory;
    }()
    
    class func getInstance() -> FurbyFactory {
        return sharedFurbyFactory
    }
    
    var furbies: [Furby] = []
    
    private init() {
        self.fillArrayWithDummyData()
    }

    func fillArrayWithDummyData() {
        self.furbies.append(Furby(name: "Kiwi", generation: 10,phrase: "wee-tah-kah-loo-loo",imageUrl: "furby1"))
        self.furbies.append(Furby(name: "Sleep", generation: 8,phrase: "wee-tah-kah-wee-loo",imageUrl: "furby2"))
        
        self.furbies.append(Furby(name: "Fire Sparke",generation: 2,phrase: "wee-tee-kah-wah-tee",imageUrl: "furby3"))
        self.furbies.append(Furby(name: "Handy Feet",generation: 2,phrase: "u-nye-loo-lay-doo?",imageUrl: "furby4"))
        self.furbies.append(Furby(name: "Motor Mouth",generation: 2,phrase: "u-nye-ay-tay-doo?",imageUrl: "furby5"))
        self.furbies.append(Furby(name: "Baby Cupcake",generation: 5,phrase: "u-nye-boh-doo?",imageUrl: "furby6"))
        self.furbies.append(Furby(name: "Little Monster",generation: 5,phrase: "u-nye-way-loh-nee-way",imageUrl: "furby7"));
        self.furbies.append(Furby(name: "Belly Button",generation: 3,phrase: "u-nye-noh-lah",imageUrl: "furby8"))
        self.furbies.append(Furby(name: "Fuzzy Pumpkin",generation: 8,phrase: "doo?",imageUrl: "furby9"))
        self.furbies.append(Furby(name: "Moon Tiger",generation: 7,phrase: "doo-dah",imageUrl: "furby10"))
    }
}
